# Pet Clinic
|Имя файла|Тип диаграммы|
|---------|-------------|
|[UseCase.puml](https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/UseCase.png)|Диаграмма вариантов использования|
|[ClassDiagramm.puml](https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/Class.png)|Диаграмма классов|
|[ActivityDiagramm.puml](https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/Component.jpg)|Диаграмма деятельности|
|[ComponentDiagramm.puml](https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/Activity.jpg)|Диаграмма компонентов|

# Диаграммы
## Диаграмма вариантов использования
<picture>
  <img src="https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/UseCase.png">
</picture>

In the Unified Modeling Language (UML), a [use case diagram](https://www.lucidchart.com/pages/uml-use-case-diagram) can summarize the details of your system's users (also known as actors) and their interactions with the system. To build one, you'll use a set of specialized symbols and connectors. An effective use case diagram can help your team discuss and represent:

Scenarios in which your system or application interacts with people, organizations, or external systems

Goals that your system or application helps those entities (known as actors) achieve

The scope of your system 

## Диаграмма классов
<picture>
  <img src="https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/Class.png">
</picture>

The UML [Class diagram](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/uml-class-diagram-tutorial/) is a graphical notation used to construct and visualize object oriented systems. A class diagram in the Unified Modeling Language (UML) is a type of static structure diagram that describes the structure of a system by showing the system's:

classes,
their attributes,
operations (or methods),
and the relationships among objects.

## Диаграмма компонентов
<picture>
  <img src="https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/Component.jpg">
</picture>

Though [component diagrams](https://www.lucidchart.com/pages/uml-component-diagram) may seem complex at first glance, they are invaluable when it comes to building your system. Component diagrams can help your team:

Imagine the system’s physical structure.

Pay attention to the system’s components and how they relate.

Emphasize the service behavior as it relates to the interface.

## Диаграмма деятельности
<picture>
  <img src="https://gitlab.com/pet-clinic2/pet-clinic/-/raw/master/images/Activity.jpg">
</picture>

[Activity diagrams](https://www.lucidchart.com/pages/uml-activity-diagram) present a number of benefits to users. Consider creating an activity diagram to:

Demonstrate the logic of an algorithm.

Describe the steps performed in a UML use case.

Illustrate a business process or workflow between users and the system.

Simplify and improve any process by clarifying complicated use cases.

Model software architecture elements, such as method, function, and operation.
